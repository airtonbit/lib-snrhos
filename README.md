lib-SNRHos
==========

Biblioteca php para integração com webservice do Sistema Nacional de Registro de Hóspedes (SNRHos) do Ministério do Turismo, que é uma exigência o envio de ficha do hóspede, checkin e checkout.
Site oficial: http://hospedagem.turismo.gov.br/
WSDL de PRODUÇÃO: http://fnrhws.hospedagem.turismo.gov.br/FnrhWs/FnrhWs?wsdl

